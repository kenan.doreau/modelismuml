## Définition des routes

1. Se connecter et récupérer les informations

S'enregistrer : POST;
https://tonagenda.com/enregistrer/

Se connecter : POST;
https://tonagenda.com/connexion/

Se déconnecter : POST;
https://tonagenda.com/deconnexion/

Récupérer la liste des contacts : GET;
https://tonagenda.com/list/

Récupérer les détails d'un contact : GET;
https://tonagenda.com/list/{id}/

2. Création

Créer un contact : POST;
https://tonagenda.com/creer/

3. Modifier un utilisateur : 

Modifier un numéro de téléphone, un email et/ou un code postal à un contact : PUT;
https://tonagenda.com/list{id}/modifier/

Ajouter un numéro de téléphone, un email et/ou un code postal à un contact : POST;
https://tonagenda.com/list{id}/ajouter/

4. Suppression

Supprimer un contact : DELETE;
https://tonagenda.com/list/{id}/supprimer/
